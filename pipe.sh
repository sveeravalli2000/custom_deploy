#!/bin/bash -l

BITBUCKET_CLONE_DIR='/opt/atlassian/pipelines/agent/build'
[[ ! -z "$APP_LOCATION" ]] && Params="--app $BITBUCKET_CLONE_DIR/$APP_LOCATION "

[[ ! -z "$API_LOCATION" ]] && Params="${Params}--api $BITBUCKET_CLONE_DIR/$API_LOCATION "

[[ ! -z "$OUTPUT_LOCATION" ]] && Params="${Params}--outputLocation $BITBUCKET_CLONE_DIR/$OUTPUT_LOCATION "

cd /bin/staticsites/
./StaticSitesClient --deploymentaction 'upload' --deploymentProvider 'Bitbucket' --apiToken $API_TOKEN "$Params"